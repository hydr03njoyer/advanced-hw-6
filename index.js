const API = "https://api.ipify.org"
const findButton = document.querySelector(".find-button");
const infoList = document.querySelector(".ip-info");
const fields = "status,message,continent,continentCode,country,countryCode,region,regionName,city,district,timezone,isp,org,as,query"

async function getIP() {
    const response = await fetch(API);

    return response.text();
}

async function findByIP() {
    const IP = await getIP()
    const response = await fetch(`http://ip-api.com/json/${IP}?fields=${fields}`);
    const data = await response.json();

    infoList.innerHTML = `
          <tr>
            <td>Continent</td>
            <td>${data.continent + " (" + data.continentCode + ")" || "Not Found"}</td>
          </tr>
          <tr>
            <td>Country</td>
            <td>${data.country || "Not Found"}</td>
          </tr>
          <tr>
            <td>Region</td>
            <td>${data.regionName + " (" + data.region + ")" || "Not Found"}</td>
          </tr>
          <tr>
            <td>City</td>
            <td>${data.city || "Not Found"}</td>
          </tr>
          <tr>
            <td>District</td>
            <td>${data.district || "Not Found"}</td>
          </tr>
    `
}

findButton.addEventListener("click", findByIP)



